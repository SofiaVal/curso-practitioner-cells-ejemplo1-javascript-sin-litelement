function storeUserData(){
	console.log("storeUserData")
  //no se envia el formulario
  event.preventDefault();
  
  var nameInput=document.getElementById("formNombre").value;
  var emailInput=document.getElementById("formEmail").value;
  
  console.log("El nombre del formulario es "+nameInput+ " y email" +emailInput);
  
  var userData = {
  	"name":nameInput,
    "email":emailInput
  }
  
  localStorage.setItem("userData", userData);
  sessionStorage.setItem("userData", userData);
  localStorage.setItem("userDataJSON", JSON.stringify(userData));
}

function getUserData(){
	console.log("getUserData");
	event.preventDefault();
  
  var userData = JSON.parse(localStorage.getItem("userDataJSON"));
  
  console.log(userData);
  
   document.getElementById("formNombre").value = userData.name;
    
   document.getElementById("formEmail").value = userData.email;
  
  //var nameInput = document.getElementById("formNombre");
 // nameInput.value = localStorage.getItem("name");
  
}